mixin Demo1{

	void m1(){
		print("Hi");
	}
}

mixin Demo2{


	void m2(){
		print("Ho");
	}
	void m1(){
		print("Hoo");
	}
}

class DemoChild with Demo2{

}

void main(){

	DemoChild obj = new DemoChild();
	obj.m1();
	obj.m2();
}
