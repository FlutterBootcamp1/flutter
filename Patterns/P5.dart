import 'dart:io';

void main(){

	int star= 1;
	int space= 4;
	int rows = 5;

	for(int i=1; i<=(2*rows)-1; i++){
		for(int j=1; j<=space; j++){
			stdout.write("  ");	
		}
		for(int k=1; k<=star; k++){
			stdout.write("* ");
		}
		if(i<rows){
			star++;
			space--;
		}else{
			star--;
			space++;
		}
		print(" ");
	}
}
		
