import 'dart:io';

void main(){

	int star = 1;
	int rows = 5;

	for(int i=1; i<=(2*rows)-1; i++){
		for(int j=1; j<=star; j++){
			stdout.write("* ");	
		}
		if(i<rows)
			star++;
		else
			star--;

		print(" ");
	}
}
			
			
