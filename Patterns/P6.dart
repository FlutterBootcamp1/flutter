import 'dart:io';

void main(){

	int row = 5;
	int space =(row*2)-2;
	int star = 1;

	for(int i=1; i<=row*2; i++){

		for(int j=1;j<=star;j++){

			stdout.write("* ");
		}
		
		for(int j=1; j<= space; j++){

			stdout.write("  ");
		}

		for(int j=1; j<=star;j++){

			stdout.write("* ");
		}
		
		if(i<5){
			
			space -= 2;
			star++;
		}
		else{
		
			space += 2;
			star--;
		}
	
		print(" ");
	}
}

