class Company{

	String? compName;
	String? loc;

	Company(this.compName,this.loc);

	void compInfo(){

		print(compName);
		print(loc);
	}
}
class Employee extends Company{

	int? empID;
	String? empName;

	Employee(this.empID,this.empName,String compName,String loc) : super(compName,loc);

	void empInfo(){
	
		print(empID);
		print(empName);
	}
}

void main(){

	Employee obj = new Employee(10,"Tejas","Google","California");

	obj.empInfo();
	obj.compInfo();
}
