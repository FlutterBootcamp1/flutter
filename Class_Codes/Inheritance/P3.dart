class Parent{

	int x=10;
	String str = "name";

	void parentM(){

		print(x);
		print(str);
	}
}
class Child extends Parent{

	int x = 20;
	String str = 'data';

	void childM(){

		print(x);
		print(str);
	}
}

void main(){

	Child obj = new Child();

	print(obj.x);
	print(obj.str);

	obj.parentM();      // parent's constructor not inherite in dart, so values of x & str will print of Child class 

	obj.childM();
}
