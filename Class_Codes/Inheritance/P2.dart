class Parent{

	int x=10;
	String str1 = "name";

	void parentM(){

		print(x);
		print(str1);
	}
}
class Child extends Parent{

	int y = 20;
	String str2 = 'data';

	void childM(){

		print(y);
		print(str2);
	}
}

void main(){

	Child obj = new Child();

	print(obj.x);
	print(obj.str1);

	print(obj.y);
	print(obj.str2);

	obj.parentM();      

	obj.childM();
}
