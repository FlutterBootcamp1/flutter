class Parent{

	Parent(){

		print("Parent Constructor");
	}
	
	call(){

		print("In call method");
	}
}
class Child extends Parent{

	Child(){

		super();
		print("Child Constructor");
	}
}
void main(){

	Child obj = new Child();
}
