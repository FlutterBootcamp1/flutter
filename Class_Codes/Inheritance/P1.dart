class Parent{

	int x =10;
	String str = "Name";

	void parentDisp(){
	
		print("In Parent");
	}
}

class Child extends Parent{

	
}

void main(){

	Child obj = new Child();
	
	print(obj.x);
	print(obj.str);

	obj.parentDisp();
}
