void main(){

	const int x = 10;
	// const int? x;  ---------- error----> must be initialized

	print(x);

	// x = 30; ---------- error --> can't be changed


}

