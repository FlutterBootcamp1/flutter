void main(){

	int x = 10;
	int y = 8;

	// &
	
	print(x & y);

	// |

	print(x | y);

	// ^ (XOR)

	print(x ^ y);

	// << (left shift)

	x = 15;
	print(x<<2);

	// >>(right shift)

	print(x>>2);
}
