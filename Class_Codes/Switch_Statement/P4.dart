
//       In dart, double data type also valid in switch case :-

void main(){

	var x = 1.0;

	switch(x){	
		case 1.0 :
			print("One");
		case 2.0 :
			print("Two");
		default :
			print("No Match");
	}
}
