class Player{

	Player(){
		print("In Constructor");
	}

}
void main(){

	//Object1 & Object2 used multiple times but, Object3 & Object4 are one time used Objects.....

        // Object1
        Player obj1 = new Player();

        //Object2
        Player obj2 = Player();

        //Object3
        new Player();

        //Object4
        Player();
}
