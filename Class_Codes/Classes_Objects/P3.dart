class Employee{
	int? empId = 10;
	String? empName = "Tejas";
	double? sal = 1.6;
	
	void empInfo(){
		print(empId);
		print(empName);
		print(sal);
	}
}
void main(){

	Employee obj1 = new Employee();

	Employee obj2 = Employee();

	obj1.empInfo();
	obj2.empInfo();

	obj1.empId = 11;
	obj2.empName = "Priyank";
	
	print("==============================");

	obj1.empInfo();
	obj2.empInfo();
}
