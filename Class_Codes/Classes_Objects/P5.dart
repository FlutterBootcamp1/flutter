class Demo{

	int x = 10;
	static int y =20;

	void printData(){
		print(x);
		print(y);
	}
}
void main(){

	Demo obj1 = new Demo();
	Demo obj2 = new Demo();

	obj1.printData();
	obj2.printData();

	print("==============");

	obj1.x = 30;
	// obj1.x = 50;                 because we cann't access static through object
 	
	Demo.y = 50;

	obj1.printData();
	obj2.printData();
	
}

