abstract class IFC{

	void material(){
	
		print("Indian material");
	}
	void taste();
}

class IndianFC implements IFC{

	void material(){
	
		print("India material");
	}
	void taste(){
	
		print("Indian taste");
	}
}

class EUFC extends IFC{

	void taste(){

		print("European taste");
	}
}

void main(){

	IndianFC obj1 = new IndianFC();
	obj1.material();
	obj1.taste();

	print("=================");

	EUFC obj2 = new EUFC();
	obj2.material();
	obj2.taste();
}
