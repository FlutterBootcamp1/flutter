abstract class Demo1{

	void m1(){

		print("In-m1 Demo1");
	}
}
abstract class Demo2{

	void m2(){

		print("In-m2 Demo2");
	}
}
class DemoChild implements Demo1, Demo2{

	void m1(){
		
		print("In-m1 DemoChild");
	}
	void m2(){
		
		print("In-m2 DemoChild");
	}
}
void main(){

	DemoChild obj = new DemoChild();
	obj.m1();
	obj.m2();
}

