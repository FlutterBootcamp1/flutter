import 'dart:io';

void main(){

	int? empId;
	String? empName;
	double? sal;

	print("Enter Employee Id :");
	empId = int.parse(stdin.readLineSync()!);

	print("Enter Employee Name :");
	empName = stdin.readLineSync();

	print("Enter Employee Salary :");
	sal = double.parse(stdin.readLineSync()!);

	print("empId : $empId");
	print("empName : $empName");
	print("sal : $sal");
}
