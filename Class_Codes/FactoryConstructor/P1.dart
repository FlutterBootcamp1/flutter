class Demo{

	Demo._private(){

		print("In Constructor");
	}
	
	factory Demo(){
	
		print("In Factory Constructor");

		return Demo._private();
	}
}

