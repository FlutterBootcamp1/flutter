abstract class Developer{

	factory Developer(String str){
			
			if(str == "Backend")
				return Backend();
			else if(str == "Fronted")
				return Fronted();
			else if(str == "Mobile")
				return Mobile();
			else
				return Other();

	}
	void lang();
}

class Backend implements Developer{

	void lang(){
		print("NodeJS/SpringBoot");
	}
}
class Fronted implements Developer{

	void lang(){
		print("ReactJS/AngularJS");
	}
}
class Mobile implements Developer{
	
	void lang(){
		print("Java/Swift/Kotlin");
	}
}
class Other implements Developer{
	
	void lang(){
		print("DevOps/Testing");
	}
}

void main(){

	Developer obj1 = new Developer("Backend");
	obj1.lang();

	Developer obj2 = new Developer("Fronted");
	obj2.lang();

	Developer obj3 = new Developer("Mobile");
	obj3.lang();

	Developer obj4 = new Developer("Other");
	obj4.lang();

}
