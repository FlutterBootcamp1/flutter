abstract class Demo{

	void property(){
		print("car,home,gold,farm");
	}
	void marry();
}
class Child extends Demo{

	void marry(){
		print("Girl");
	}
	void career(){
		print("PM");
	}
}
void main(){

	Child obj = new Child();
	obj.property();
	obj.marry();
	obj.career();
}

