class Demo{

	int? _x;
	String? _str;
	double? _sal;

	Demo(this._x,this._str,this._sal);

	int? getX(){

		return _x;           //---------> Way 1
	}
	String? get getStr{

		return _str;         //--------> Way 2
	}
	
	get getSal => _sal;          //--------> Way 3 (Preffered)

}
