mixin Demo1{

		void fun1(){
			
			print("In Demo-fun1");
		}

		void fun2();
}
mixin Demo2{

		void fun3(){
	
			print("In Demo-fun3");
		}
		
		void fun4();
}
class Demochild with Demo1,Demo2{
	
		void fun2(){

			print("In Demochild-fun2");
		}
	
		void fun4(){

			print("In Demochild-fun4");
		}
}

void main(){

	Demochild obj = new Demochild();

	obj.fun1();
	obj.fun2();
	obj.fun3();
	obj.fun4();

}
