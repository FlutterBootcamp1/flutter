mixin Demo{

	Demo(){

		  //----------------------> Error : mixin have only by-default constructors, we cann't explicitly
	}

	void fun(){

	}
}

void main(){

	Demo obj = new Demo();  //--------------------> Error : mixin is abstract class, so cann't create object
	
	obj.fun();          
}

	
