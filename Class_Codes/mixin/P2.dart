mixin Demo1{

	void m1(){

		print("In-m1 Demo1");
	}
}
mixin Demo2{

	void m1(){

		print("In-m1 Demo2");
	}
}
class Demo with Demo1,Demo2{

	
}

void main(){

	Demo obj = new Demo();
	obj.m1();
}
