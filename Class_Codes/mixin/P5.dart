mixin Demo1{

	void fun1(){

		print("In Demo1-fun1");
		}
}

mixin Demo2 on Demo1{         //-------------> Error : 

	void fun2(){

		print("In Demo2-fun2");
	}
}

class Demochild with Demo2{


}


void main(){

	Demochild obj = new Demochild();
	obj.fun1();
	obj.fun2();
}
