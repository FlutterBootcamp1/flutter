
import 'dart:io';

Future<void> main() async{

	print("Start");
	print(await getOrderMessage());
	print("end");
}

Future<String> getOrderMessage() async{

	var order = await getOrder();
	return "Your order is $order";
}

Future<String?> getOrder(){

	return Future.delayed(Duration(seconds:5),()=> placeOrder());
}

Future<String?> placeOrder() async{

	Future.delayed(Duration(seconds:2));

	print("Enter your order :");
	String? ord = await stdin.readLineSync();
	
	return ord;
}
