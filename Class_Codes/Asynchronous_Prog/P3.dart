void fun2(){

	for(int i=0; i<5; i++){
	
		print("Fun2-First");
	}

	Future.delayed(Duration(seconds:5));
	
	for(int i=0; i<5; i++){

		print("Fun2-Second");
	}
}

void fun1(){

	for(int i=0; i<5; i++){

		print("Fun1");
	}
}

void main(){

	print("Start main");

	fun1();
	fun2();

	Future.delayed(Duration(seconds:5),()=>print("Tejas"));
	
	print("End main");
}

