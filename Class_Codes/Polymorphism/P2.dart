class Demo{

	int x =10;
	int y =20;

	void disp(int x){

		this.x = x;
		print(x);
	}
	void disp(int x, int y){       // This is Overloading Example & It's not valid in Dart
				       // Due to same object name

		this.x = x;
		this.y = y;
		print(x);
		print(y);
	}
}
void main(){

	Demo obj = new Demo();
	obj.disp(10);
	obj.disp(20,30);
}
